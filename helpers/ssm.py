import boto3 as boto


class SSM(object):

    def __init__(self, path, region='eu-central-1'):
        self._region = region
        self.client = boto.client('ssm', region_name=self._region)
        self.parameters = self._get_parameters_by_path(path)

    def _get_parameters_by_path(self, path, recursive=True,
                                with_decryption=True):
        params = self.client.get_parameters_by_path(
            Path=path, Recursive=recursive, WithDecryption=with_decryption
        )
        return {item['Name']: item['Value'] for item in params['Parameters']}

    def get_param(self, name):
        return self.parameters.get(name, None)
