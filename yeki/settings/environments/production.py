import os
from helpers.ssm import SSM

ssm = SSM('/yeki/production')
SECRET_KEY = ssm.get_param(
    '/yeki/production/app/secret_key'
)

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/2.2/howto/deployment/checklist/
ALLOWED_HOSTS = [
    'yeki-platform-production.eba-qyh3xqjm.eu-central-1.elasticbeanstalk.com',
    'yeki.org',
    'www.yeki.org',
]

# Database
# https://docs.djangoproject.com/en/2.2/ref/settings/#databases
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': os.environ['RDS_DB_NAME'],
        'USER': os.environ['RDS_USERNAME'],
        'PASSWORD': os.environ['RDS_PASSWORD'],
        'HOST': os.environ['RDS_HOSTNAME'],
        'PORT': os.environ['RDS_PORT'],
    }
}

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = False
